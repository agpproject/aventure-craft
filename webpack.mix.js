const mix = require('laravel-mix');

mix
    .js('resources/js/app.js', 'public/js/dist/')
    .js('resources/js/password-form.js', 'public/js/dist/')
    .sass('resources/sass/main.scss', 'public/css/');
