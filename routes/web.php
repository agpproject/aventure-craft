<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// AUTH ROUTES
Route::prefix('/auth')->name('auth.')->group(function ()
{
    $auth = "AuthController";

    Route::get('/logout', "$auth@logout")->name('logout')->middleware('auth');

    Route::post('/login', "$auth@login")->name('login');
});

// GUIDE ROUTES
Route::prefix('/guide')->name('guide.')->group(function ()
{
    $guide = "GuideController";

    Route::get('/', "$guide@home")->name('home');
    Route::get('/server-rules', "$guide@server_rules")->name('server-rules');
});

// HELPERS ROUTES
Route::prefix('/helpers')->name('helpers.')->group(function ()
{
    $helpers = 'HelpersController';

    Route::get('/profile/activation/{type?}', "$helpers@profile_activation")->name('profile.activation');

    Route::get('/profile/delete/{type?}', "$helpers@profile_delete")->name('profile.delete');

    Route::get('/email/activation', "$helpers@email_activation")->name('email.activation');

    Route::get('/reset-password', "$helpers@reset_password")->name('reset.password');
    Route::get('/reset-password/expire', "$helpers@reset_password_expire")->name('reset.password.expire');
    Route::get('/reset-password/end', "$helpers@reset_password_end")->name('reset.password.end');
});

// MAIN ROUTES
$main = "MainController";

Route::get('/', "$main@home")->name('home');
Route::get('/mentions-legales', "$main@mentions")->name('mentions');
Route::get('/download', "$main@download")->name('download');

Route::post('/contact/send', "$main@contact")->name('contact');

// USER ROUTES
Route::prefix('/user')->name('user.')->group(function ()
{
    $user = "UserController";

    Route::get('/profile/activation/{token}', "$user@activation")->name('activation');
    Route::get('/password-reset', "$user@password_reset")->name('reset.password.view');
    Route::get('/password-reset/{token}', "$user@password_reset_end")->name('reset.password.end');

    Route::post('/profile/inscription', "$user@inscription")->name('inscription');
    Route::post('/password-reset/send', "$user@password_reset")->name('reset.password');

    Route::put('/profile/update', "$user@update")->name('update');
    Route::put('/password/update', "$user@password_reset")->name('reset.password.update');

    Route::match(['get', 'delete'], '/profile/delete/{token?}', "$user@delete")->name('delete');
});
