import { parallax_setup } from './helpers/parallax-setup'
import { active_nav_item } from './helpers/active-nav';
import { control_password } from './helpers/password';
import { login } from './helpers/login';
import { profile_form } from './helpers/profile';
import { contact_form } from './helpers/contact';

let log_form = $('#logForm');

parallax_setup();
active_nav_item($('nav#menu li.nav-item'), ['guide']);
contact_form('#contactForm');

$('nav#menu button').click(function () {
    let modal = $(this).data('target');
    $(modal + ' div[role=alert]').remove();
});

if (log_form.length !== 0) {
    $('#loginModal').on('shown.bs.modal', function () {
        $('#logEmail').trigger('focus');
        log_form.trigger('reset');
    });

    login(log_form);
}

if ($('#inscriptForm').length !== 0) {
    $('#inscriptModal').on('shown.bs.modal', function () {
        $('#username').trigger('focus');
    });

    profile_form($('#inscriptForm'), true);
    control_password('#inscriptForm', $('#passwordController'), $('#password'), $('#confPassword'), $('#inscriptForm button[type=submit]'));
}

if ($('#profileForm').length !== 0) {
    profile_form($('#profileForm'));
    let password = $('#profileForm input[type=password]');
    let controller = $('#passwordController');

    controller.hide();

    password.on('focus', function ()
    {
        controller.show();
        control_password('#profileForm', $('#passwordController'), $('#password'), $('#confPassword'), $('#profileForm button[type=submit]'));
    }).on('focusout', function ()
    {
        if ($('#password').val() === '' && $('#confPassword').val() === '') {
            controller.hide();
            $('#profileForm input[type=password]').off('keyup');
            $('#passwordController #low').toggleClass('bg-danger');
            $('#passwordController p').text('');
        }
    });
}

let imgUrl = document.querySelector('#maps-button').getAttribute('data-img');

document.querySelector('#maps-button').addEventListener('mouseenter', function () {
    document.querySelector('#maps-button > img').setAttribute('src', `${imgUrl}/map-on.png`);
});
document.querySelector('#maps-button').addEventListener('mouseleave', function () {
    document.querySelector('#maps-button > img').setAttribute('src', `${imgUrl}/map-off.png`);
});
/*
$('#maps-button').click(function () {
    let url = document.querySelector('#mini-maps').getAttribute('src');
    let modal = document.querySelector('#maps-modal');
    let iframe = document.querySelector('#maps-modal > iframe');
    let closeButton = document.querySelector('#close-modal');
    let body = document.querySelector('body');

    modal.style.display = 'block';
    iframe.setAttribute('src', url);
    body.style.overflow='hidden';

    modal.addEventListener('click', function () {
        modal.style.display = 'none';
        body.style.overflow = 'auto';
    });
    closeButton.addEventListener('click', function () {
        modal.style.display = 'none';
        body.style.overflow = 'auto';
    });
});
*/
