'use strict';

import { control_password } from './helpers/password';

control_password('#passwordForm', $('#passwordControllerBis'), $('#passwordForm #password'), $('#passwordForm' +
    ' #confPassword'), $('#passwordForm button[type=submit]'));
