'use strict';

import { alert_box_modal, input_not_validate } from './alert';
import { minor } from './minor';

export function profile_form(profile_form, inscription = false)
{
    profile_form.submit(function (e)
    {
        e.preventDefault();

        $((inscription) ? '#inscriptionModal' : '#profileModal' + ' div[role=alert]').remove();

        let submit = $(((inscription) ? '#inscriptForm' : '#profileForm') + ' button[type=submit]');
        let modal = (inscription) ? $('#inscriptModal') : $('#profileModal');
        let username = $('#username').val();
        let email = $('#email').val();
        let birthday = $('#birthday').val();
        let password = $('#password').val();
        let confPassword = $('#confPassword').val();
        let data = {
            'user': (inscription) ? null : $('#user-id').val(),
            'username': username,
            'email': email,
            'birthday': birthday,
            'password': password,
            'confPassword': confPassword
        };

        $('#inscriptModal div[role=alert]').remove();

        let fields_test = (inscription) ? username && email && birthday && password && confPassword : true;

        if (fields_test) {
            if (minor(birthday)) {
                let message = (inscription) ? "Vous devez avoir au moins 18 ans pour vous inscrire" : "Vous devez avoir au moins 18 ans";
                alert_box_modal('warning', profile_form, modal, message);
            } else {
                submit.attr('disabled', true).text('En Chargement...');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': profile_form.children('input[name=_token]').val()
                    }
                });

                $.ajax({
                    url: profile_form.attr('action'),
                    type: (inscription) ? 'post' : 'put',
                    data: data
                })
                    .done(data => {
                        if (inscription)
                            window.location.href = data.success;
                        else {
                            alert_box_modal('success', profile_form, modal, "Votre compte a été mis à jour");
                            submit.attr('disabled', false).text('Mettre à jour');
                            if (inscription)
                                profile_form.trigger('reset');
                        }
                    })
                    .fail(xhr => {
                        console.log(xhr);
                        let errors = JSON.parse(xhr.responseText).errors;
                        alert_box_modal('warning', profile_form, modal, (errors) ? errors : "Une erreur est survenue");
                        submit.text((inscription) ? "S'inscrire" : 'Mettre à jour');
                        if (!(username && email && birthday && password && confPassword) && inscription)
                            submit.attr('disabled', true);
                    });
            }
        } else {
            alert_box_modal('warning', profile_form, modal, 'Veuillez remplir tous les champs obligatoires');
            input_not_validate('#inscriptForm', 'not-validate');
        }
    });
}
