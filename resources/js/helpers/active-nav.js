'use strict';

export function active_nav_item(nav_item, group_page_filter = [])
{
    let current_url = window.location.href;
    let regex_test = null;

    if (group_page_filter.length === 0)
        regex_test = RegExp('null');
    else {
        let test = '';
        $.each(group_page_filter, (index, page_filter) => {
            if (test === '') {
                test += '(^.+' + page_filter + '$)';
                test += '|(^.+' + page_filter + '.+$)';
            } else {
                test += '|(^.+' + page_filter + '$)';
                test += '|(^.+' + page_filter + '.+$)';
            }
        });
        regex_test = RegExp(test);
    }

    $.each(nav_item, (index, li) => {
        $(li).removeClass('active');
        let href = $(li).children('a.nav-link').attr('href');
        let regex_url = RegExp(href);

        if (!regex_test.test(current_url)) {
            if (current_url === href || current_url === href + '/') {
                if (index - 1 >= 0)
                    $(nav_item[index - 1]).removeClass('active');

                $(li).addClass('active');
            }
        } else if (regex_url.test('^' + current_url + '+.$')) {
            if (index - 1 >= 0)
                $(nav_item[index - 1]).removeClass('active');

            $(li).addClass('active');
        }
    });
}
