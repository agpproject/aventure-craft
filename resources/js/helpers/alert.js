'use strict';

export function alert_box_modal(alert_type, form, modal, messages)
{
    let alert = $('<div>').attr('role', 'alert');

    if (typeof messages === 'object') {
        let ul = $('<ul>');
        $.each(messages, (index, message) => {
            $.each(message, (index, text) => {
                let li = $('<li>').text(text);
                ul.append(li);
            });
        });

        alert.addClass('alert alert-' + alert_type).append(ul);
        form.before(alert);
    } else {
        alert.addClass('alert alert-' + alert_type).text(messages);
        form.before(alert);
    }
    modal.animate({scrollTop: 0}, 'slow');
}

export function input_not_validate(input_id, not_validate_class)
{
    let inputs = $(input_id + ' input[type=text], ' + input_id + ' input[type=date], ' + input_id + ' input[type=email], ' + input_id + ' input[type=password], ' + input_id + ' textarea');

    $.each(inputs, (index, input) => {
        $(input).removeClass(not_validate_class);
        if ($(input).val() === '')
            $(input).addClass(not_validate_class);
    });
}
