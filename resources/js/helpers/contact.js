'use strict';

import { alert_box_modal, input_not_validate } from './alert';

export function contact_form(contact_form_id)
{
    let email = $('#contactEmail').val();
    let subject = $('#contactSubject').val();
    let message = $('#contactMessage').val();
    let form = $(contact_form_id);
    let modal = $('#contactModal');
    let submit_button = $(contact_form_id + ' button[type=submit]');
    let submit_button_text = submit_button.text();

    form.submit(function (e) {
        e.preventDefault();
        submit_button.text('Envois en cours...').attr('disabled', true);

        $('#contactModal div[role=alert]').remove();

        if (email === '' || subject === '' || message === '') {
            input_not_validate(contact_form_id, 'not-validate');
            alert_box_modal('warning', form, modal, 'Veuillez remplir tous les champs');
            submit_button.text(submit_button_text).attr('disabled', false);
        } else {
            let data = {
                'email': email,
                'subject': subject,
                'message': message
            };

            $.ajaxSetup({
                'headers': {
                    'X-CSRF-TOKEN': $(contact_form_id + ' input[name=_token]').val()
                }
            });

            $.post(form.attr('action'), data)
                .done((data) => {
                    alert_box_modal('success', form, modal, data.success);
                    submit_button.text(submit_button_text).attr('disabled', false);
                })
                .fail((xhr) => {
                    let errors = xhr.responseJSON.errors;

                    if (errors)
                        alert_box_modal('warning', $(contact_form_id), modal, errors);
                    else
                        alert_box_modal('warning', $(contact_form_id), modal, 'Une erreur est survenue');

                    submit_button.text(submit_button_text).attr('disabled', false);
                });
        }
    });
}
