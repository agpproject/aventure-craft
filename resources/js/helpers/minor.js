'use strict';

export function minor(date)
{
    let now = new Date();
    let major_date = new Date((now.getFullYear() - 18) + '-' + (now.getMonth() + 1) + '-' + now.getDate());
    let birthday = new Date(date);
    return birthday > major_date;
}
