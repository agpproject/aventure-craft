import { alert_box_modal } from './alert';

export function login(log_form)
{
    log_form.submit(function (e)
    {
        e.preventDefault();

        let submit = $('#logForm button[type=submit]');
        let modal = $('#loginModal');
        let email = $('#logEmail').val();
        let password = $('#logPassword').val();

        $('#loginModal div[role=alert]').remove();


        let data = {
            'email': email,
            'password': password
        };

        if (email && password) {
            submit.attr('disabled', true).text('En connexion...');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': log_form.children('input[name=_token]').val()
                }
            });
            $.post(log_form.attr('action'), data)
                .done(data => {
                    console.log(data);
                    if (data.success)
                        window.location.href = data.success;
                    else {
                        alert_box_modal('warning', log_form, modal, "Une erreur est survenue");
                        submit.attr('disabled', false).text('Se Connecter');
                    }
                })
                .fail(xhr => {
                    let errors = JSON.parse(xhr.responseText);
                    let message = (errors.errors) ? "Adresse e-mail et/ou mot de passe incorrect" : "Une erreur est survenue";
                    alert_box_modal('warning', log_form, modal, message);
                    submit.attr('disabled', false).text('Se Connecter');
                });
        } else {
            alert_box_modal('warning', log_form, modal, "Adresse et/ou mot de passe incorrect");
        }
    });
}
