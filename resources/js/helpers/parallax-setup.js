'use strict';

export function parallax_setup()
{
    let content = $('.parallax-window');
    content.parallax({
        imageSrc: content.data('image-src')
    });
}

