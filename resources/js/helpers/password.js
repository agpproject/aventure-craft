'use strict';

export function control_password(form_id, controller, pswd, confPswd, submit_button)
{
// Initialize
    if (pswd.val().length < 8) {
        controller.children('#low').toggleClass('bg-danger');
        controller.children('p').toggleClass('text-danger').text('Le mot de passe doit faire au moins 8 caractères');
        submit_button.attr('disabled', true);
    }


// Password control
    $(form_id + ' input[type=password]').on('keyup', function () {

        let regex = RegExp('[A-Z0-9@$%!£]{3}');

        if (pswd.val().length < 8) {
            controller.children('#low').addClass('bg-danger');
            controller.children('#medium').removeClass('bg-warning');
            controller.children('#strong').removeClass('bg-success');
            controller.children('p').removeClass('text-warning');
            controller.children('p').removeClass('text-success');
            controller.children('p').addClass('text-danger').text('Le mot de passe doit au moins faire 8 caractères');
            submit_button.attr('disabled', true);
        } else if (pswd.val() !== confPswd.val()) {
            controller.children('#low').addClass('bg-danger');
            controller.children('#medium').removeClass('bg-warning');
            controller.children('#strong').removeClass('bg-success');
            controller.children('p').removeClass('text-warning');
            controller.children('p').removeClass('text-success');
            controller.children('p').addClass('text-danger').text('Les mots de passe ne sont pas identiques');
            submit_button.attr('disabled', true);
        } else if (pswd.val().length >= 8 && pswd.val().length < 10) {
            controller.children('#low').removeClass('bg-danger');
            controller.children('#medium').addClass('bg-warning');
            controller.children('#strong').removeClass('bg-success');
            controller.children('p').removeClass('text-danger');
            controller.children('p').removeClass('text-success');
            controller.children('p').addClass('text-warning').text('Mot de passe valide');
            submit_button.attr('disabled', false);
        } else if (regex.test(pswd.val()) || pswd.val().length >= 10) {
            controller.children('#low').removeClass('bg-danger');
            controller.children('#medium').removeClass('bg-warning');
            controller.children('#strong').addClass('bg-success');
            controller.children('p').removeClass('text-danger');
            controller.children('p').removeClass('text-warning');
            controller.children('p').addClass('text-success').text('Mot de passe fort');
            submit_button.attr('disabled', false);
        }
    });
}
