<?php

return [
    'custom' => [
        'email' => [
            'unique' => "Cette adresse e-mail est déjà utilisée"
        ],
        'username' => [
            'unique' => "Ce nom d'utilisateur est déjà utilisé"
        ]
    ]
];
