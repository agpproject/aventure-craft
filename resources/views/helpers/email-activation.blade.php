@extends('layouts.helpers')

@section('title', "Un e-mail vous a été envoyé !")

@section('content')
    <h1>Un e-mail vous a été envoyé pour valider votre compte !</h1>
@endsection
