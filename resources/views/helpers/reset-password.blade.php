@extends('layouts.helpers')

@section('title', "Rénitialisation de mot de passe")

@section('content')
    @if($expire)
        <h1>Cette demande a expirée !</h1>
        <h3>Le refaire, <a href="{{ route('user.reset.password.view') }}">Mot de passe oublié</a>.</h3>
    @elseif($end)
        <h1>Votre mot de passe vient d'être réinitialisé !</h1>
        <h3>Un e-mail vient de vous être envoyé pour faire état du changement.</h3>
    @else
        <h1>Vous venez de faire une demande de réinitialisation de mot de passe.</h1>
        <h3>Un e-mail vient de vous être envoyé afin de terminer la réinitialisation.</h3>
    @endif
@endsection
