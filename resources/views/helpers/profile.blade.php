@extends('layouts.helpers')

@if($type === 'activation')
    @section('title', "Votre compte est activé !")
@elseif($type === 'expire')
    @section('title', "L'e-mail d'activation est expiré !")
@elseif($type === 'delete-first')
    @section('title', "Vous avez fait une demande de suppression !")
@elseif($type === 'delete-expire')
    @section('title', "Le lien de suppression est supprimée !")
@elseif($type === 'delete-end')
    @section('title', "Votre aventure se termine ici !")
@endif

@section('content')
    @if($type === 'activation')
        <h1>Votre compte est désormais activé ! =)</h1>
        <h3>Vous pouvez désormais vous connecter sur Aventure-Craft.</h3>
    @elseif($type === 'expire')
        <h1>Le lien d'activation est expiré !</h1>
        <h3>Un nouvel e-mail vient de vous être envoyé sur votre adresse e-mail.</h3>
    @elseif($type === 'delete-first')
        <h1>Une demande de suppression de compte vient d'être faite !</h1>
        <h3>Un e-mail pour confirmer la demande vient de vous être envoyé.</h3>
    @elseif($type === 'delete-expire')
        <h1>Le lien de suppression est expiré !</h1>
        <h3>Veuillez recommencer la demande de suppression.</h3>
    @elseif($type === 'delete-end')
        <h1>Votre compte a été supprimé ! Votre aventure est terminée.</h1>
        <h3>Merci d'avoir été parmis nous.</h3>
    @endif
@endsection
