@extends('layouts.guide')

@section('main')
    <section class="row p-3 bg-white">
        <div class="col">
            <div class="row">
                <div class="col">
                    <h1><strong>Le Guide</strong></h1>
                </div>
            </div>
            <hr>
            <article class="row">
                <main class="col">
                    <p>
                        Bienvenue sur <strong>Aventure-Craft</strong>,

                        Ce petit questionnaire répondra à quelques

                        questions concernant le Serveur. Nous espérons

                        que cela vous aidera à bien débuté votre aventure

                        Nous restons à votre écoute en jeu ou

                        sur le TeamSpeak.
                    </p>
                </main>
            </article>
            <hr>
            <article class="row">
                <div class="col">
                    <header>
                        <h2><strong>Pourquoi venir sur Aventure-Craft ?</strong></h2>
                    </header>
                    <main>
                        <p>
                            Aventure-Craft c'est 5 ans de travail,

                            avec une petite équipe pour réalisée des cartes une histoire et de nombreux défit à relever. Ce serveur est de type RolePlay.

                            Animée par des Quêtes, Donjons, Boss et Cinématiques nous vous proposons une véritable aventure.

                            Vous pourrez avec vos amis, partir à la conquête du monde en créant votre Clan. Devenez un Héro en montant votre Classes et votre Professions.

                            Bien plus encore vous attendent...
                        </p>
                    </main>
                </div>
            </article>
            <hr>
            <article class="row">
                <div class="col">
                    <header>
                        <h2><strong>Quelles sont vos outils?</strong></h2>
                    </header>
                    <main>
                        <p>
                            Notre serveur dispose de divers fonctions, dont un Discord,

                            Sans Launcher, ni Mods: connectez vous facilement.

                            Nous vous proposons un <strong>RessourcePack</strong>

                            adapté au serveur, il vous sera conseillé d'utilisé <strong>OptiFine</strong> pour en profité pleinement.

                            Le Site est également un soutiens pour vous tenir informé des nouveautés. Ainsi que notre Discord pour toutes questions complémentaires.

                            Pour finir, nous restons à votre dispositions en jeu.
                        </p>
                    </main>
                </div>
            </article>
            <hr>
            <article class="row">
                <div class="col">
                    <header>
                        <h2><strong>Comment nous rejoindre ?</strong></h2>
                    </header>
                    <main>
                        <p>
                            Pour nous rejoindre, inscrivez vous sur notre site en cliquant sur le bouton Inscription.

                            Une fois inscrit, créé votre Candidature en cliquant sur Postuler. Elle sera examiné sous 12H par notre Staff.

                            ATTENTION: Nous pouvons vous demandez de la refaire si elle ne correspond pas.

                            Une fois Validée, téléchargez le RessourcePack depuis la section Média du Site.
                        </p>
                    </main>
                </div>
            </article>
            <hr>
            <article class="row">
                <div class="col">
                    <header>
                        <h2><strong>Serais-je avec des amis ?</strong></h2>
                    </header>
                    <main>
                        <p>
                            Ce serveur a été créé pour joué Seul ou à plusieurs, avec vos amis.

                            Que se soit via le commerce, les Clans avec la Conquête de vos Terres ou les Donjons, Quêtes et autres.

                            Sans limite, le serveur suit une histoire dans la quel vous aurez un impacte.

                            Nous rajoutons du contenu chaque mois via les Patchs.
                        </p>
                    </main>
                </div>
            </article>
            <hr>
            <article class="row">
                <main class="col">
                    <p>
                        <strong>Aventure-Craft vous attend.

                        Rejoignez nous dès maintenant,

                        et Merci de votre venue.</strong>
						<a class="d-flex justify-content-center" href="{{ route('download') }}"><img class="btn-img" src="{{ asset('images/slider-jouer.png') }}" alt="slider-jouer"></a>
                    </p>
                </main>
            </article>
        </div>
    </section>
@endsection
