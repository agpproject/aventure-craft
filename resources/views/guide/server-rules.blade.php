@extends('layouts.guide')

@section('title', "Le Code des Aventuriers d'Averall")

@section('main')
    <!-- Server rules -->
    <section class="row bg-white">
        <div class="col p-3">
            <div class="row">
                <div class="col">
                    <h1>Le Code des Aventuriers d'Averall</h1>
                </div>
            </div>
            <hr>
            <article class="row">
                <main class="col">
                    <p>
                        Toute <strong>décision</strong> d’un <strong>Administrateur ou membre du Staff</strong>, qu’elle soit guidée par un article du règlement ou non, <strong>devra être respectée</strong> par les membres du serveur. Le <strong>règlement</strong> peut <strong>changer</strong> à tout moment, <strong>vous</strong> êtes donc <strong>priés</strong> de vous tenir <strong>informés régulièrement</strong>. En vous <strong>connectant</strong> sur le serveur vous <strong>certifiez explicitement avoir lu et accepté le règlement</strong>.
                    </p>
                </main>
            </article>
            <hr>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 1</h3>
                    </header>
                    <main>
                            <p>
                                Toute <strong>action</strong> telle qu'elle soit pouvant <strong>porter atteinte</strong> à la <strong>réputation</strong>, <strong>l'intégrité</strong>, la <strong>solvabilité</strong>, <strong>l'équilibre</strong> ou au bon <strong>fonctionnement</strong> du serveur est <strong>prohibée</strong> et sera <strong>sanctionnée</strong>.
                            </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 2</h3>
                    </header>
                    <main>
                        <p>
                            Les <strong>insultes</strong>, <strong>propos racistes</strong> et <strong>discriminatoires</strong>, <strong>manques de respect</strong> envers un <strong>joueur</strong> ou un <strong>membre du Staff</strong> et <strong>noms/descriptions</strong> de faction <strong>incorrectes</strong> et/ou <strong>obscènes</strong> seront sévèrement <strong>sanctionnées</strong>.
                        </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 3</h3>
                    </header>
                    <main>
                        <p>
                            Les <strong>usebugs</strong> sont <strong>interdits</strong> et <strong>passibles de lourdes sanctions</strong> en fonction de la gravité. Signaler un usebug à un <strong>membre du Staff</strong> et créé un article sur le forum pour le <strong>signaler</strong> !
                        </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 4</h3>
                    </header>
                    <main>
                        <p>
                            Le <strong>langage</strong> devra être <strong>correct</strong> et le <strong>SMS</strong> est explicitement <strong>déconseillé</strong>. Les <strong>fautes ne sont pas proscrites</strong>, personne n’est parfait, mais nous vous en demandons un minimum de façon à ce que le chat soit lisible par tous.
                        </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 5</h3>
                    </header>
                    <main>
                        <p>
                            Le <strong>flood</strong> dans le canal de chat est <strong>interdit</strong> et peut être <strong>puni d’un kick immédiat et d’un mute si récidives enfantines</strong>.
                        </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 6</h3>
                    </header>
                    <main>
                        <p>
                            Les <strong>accusations de cheat</strong> en jeu doivent être <strong>valable</strong> et <strong>prouvable</strong>. Toutes <strong>plaintes</strong> devra donc être <strong>signalées</strong> sur le forum. Il est <strong>obligatoire</strong> d’avoir des <strong>preuves</strong> (vidéo ou screenshots significatifs). Tout <strong>abus</strong> sera <strong>sanctionné</strong>.
                        </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 7</h3>
                    </header>
                    <main>
                        <p>
                            Aventure-Craft <strong>s’engage</strong> à faire <strong>tout le possible</strong> pour que le <strong>serveur</strong> soit <strong>stable</strong> et soit <strong>ouvert</strong> tous les jours de l’année.
                        </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 8</h3>
                    </header>
                    <main>
                        <p>
                            Les <strong>administrateurs ne sont pas responsables</strong> des <strong>pertes</strong> dues aux <strong>bugs divers ou aux rollbacks potentiels</strong>, les <strong>dégâts matériaux</strong> dus aux griefs <strong>ne peuvent pas être réparés</strong>. Dans ces cas <strong>signalez</strong> sur le Forum votre situation. Nous <strong>réglerons</strong> au cas par cas les situations <strong>sans obligation de restitution</strong> d'inventaire ou autre en cas de perte. <strong>Cependant</strong> Aventure-Craft <strong>s’engage</strong> à faire <strong>tout son possible</strong> pour que ce genre de situations <strong>ne se produisent pas</strong>.
                        </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 9</h3>
                    </header>
                    <main>
                        <p>
                            Si vous <strong>défendez</strong> quelqu’un qui a <strong>délibérément enfreint</strong> l’une des présentes <strong>règles</strong>, vous <strong>encourrez des sanctions</strong>.
                        </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 10</h3>
                    </header>
                    <main>
                        <p>
                            Tous les <strong>cheats/hacks</strong> existants pour Minecraft sont <strong>interdits</strong> : FlyMod, X-Ray, Speedhacks, Pack de Texture cheaté etc. Leurs <strong>utilisateurs</strong> seront <strong>bannis sans préavis</strong>. Les seuls mods <strong>autorisés</strong> sont : Rei`s minimap, MCPatcher et optifine.
                        </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 11</h3>
                    </header>
                    <main>
                        <p>
                            La <strong>trahison</strong>, le <strong>pillage</strong>, peuvent être <strong>toléré</strong> mais <strong>aucunement le harcèlement</strong>. Tous les <strong>abus</strong> repéré seront <strong>sanctionnées</strong>.
                        </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 12</h3>
                    </header>
                    <main>
                        <p>
                            La <strong>pub</strong> pour tout <strong>autres serveurs</strong> est strictement <strong>interdite</strong>, citer le <strong>nom</strong> d'un autre serveur est considérée comme de la <strong>pub</strong>. Vous avez le droit de jouer sur un autre serveur, mais par respect n’en parlez pas en jeu.
                        </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 13</h3>
                    </header>
                    <main>
                        <p>
                            Les <strong>membres du Staff</strong> ne <strong>givent rien</strong> et <strong>ne déplacent pas les joueurs</strong>, toute <strong>demande</strong> est <strong>interdite</strong> et pourra être <strong>sanctionnée</strong> si récidive.
                        </p>
                    </main>
                </div>
            </article>
            <article class="row">
                <div class="col">
                    <header>
                        <h3>Article 14</h3>
                    </header>
                    <main>
                        <p>
                            Demander à être <strong>op</strong> ou à <strong>intégrer</strong> le <strong>Staff</strong> est strictement <strong>interdit</strong>. Toute récidive sera <strong>sanctionnée</strong>.
                        </p>
                    </main>
                </div>
            </article>
            <hr>
            <article class="row">
                <main class="col">
                    <p>
                        Merci de respecter ce code sur nos support, le Site, le Forum, le TeamSpeak et en jeu. Le Staff reste à votre disposition.
                    </p>
                </main>
            </article>
        </div>
    </section>
    <!-- End server rules -->
@endsection
