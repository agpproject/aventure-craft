@extends('layouts.mail')

@section('content')
    <tr>
        <td align="center" style="padding: 20px;">
            <div style="width: 80%;margin: 0 auto;">
                <h1>Une demande de réinitialisation de mot de passe à été effectuée !</h1>
                <p>Afin de réinitialiser votre mot de passe, veuillez cliquer sur le bouton ci-dessous.</p>
                <p>Le lien est valable 30 minutes. <span style="color: cornflowerblue;">Si vous n'êtes pas à l'origine de cette demande, sécurisé votre boit e-mail.</span></p>
            </div>
        </td>
    </tr>
    <tr>
        <td align="center" height="100" width="400">
            <figcaption>
                <a traget="_blank" href="{{ route('user.reset.password.end', ['token' => $token]) }}" style="font-size: 1rem;padding: 20px 10px;background-color: #ffba42;color: black;display: inline-block;">Réinitialiser votre mot de passe</a>
            </figcaption>
            <small style="color: grey;display: block;margin-top: 5px;">
                Si cet e-mail ne vous est pas destiné, merci de ne pas en tenir compte.
            </small>
        </td>
    </tr>
@endsection
