@extends('layouts.mail')

@section('title', "Activation du compte")

@section('content')
    <tr>
        <td align="center" style="padding: 20px;">
            <div style="width: 80%;margin: 0 auto;">
                <h1>Bienvenue sur Aventure-Craft !</h1>
                <p>Afin d'activer votre compte veuillez cliquer sur le bouton ci-dessous.</p>
                <p>Le lien est valable 30 minutes.</p>
            </div>
        </td>
    </tr>
    <tr>
        <td align="center" height="100" width="400">
            <figcaption>
                <a traget="_blank" href="{{ route('user.activation', ['token' => $token]) }}" style="font-size: 1rem;padding: 20px 10px;background-color: #ffba42;color: black;display: inline-block;">Activer mon compte
                    !</a>
            </figcaption>
            <small style="color: grey;display: block;margin-top: 5px;">
                Si cet e-mail ne vous est pas destiné, merci de ne pas en tenir compte.
            </small>
        </td>
    </tr>
@endsection
