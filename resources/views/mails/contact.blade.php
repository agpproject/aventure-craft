<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Un utilisateur a pris contact</title>
</head>
<body>
    <h1>Un utilisateur vient de prendre contact</h1>
    <p><strong>E-mail de l'utilisateur :</strong> {{ $email }}</p>
    <p><strong>Sujet :</strong> {{ $subject }}</p>
    <p><strong>Message :</strong></p>
    <p style="max-width: 400px;margin-left: 20px;">
        {{ $text }}
    </p>
</body>
</html>
