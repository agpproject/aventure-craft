@extends('layouts.mail')

@section('title', $subject)

@section('content')
    <tr>
        <td align="center" style="padding: 20px;">
            <div style="width: 80%;margin: 0 auto;">
            @if($type === 'reset-end')
                <h1>Votre mot de passe vient d'être réinitialisé !</h1>
                <p><span style="color: cornflowerblue;">Si vous n'êtes pas à l'origine de cette demande, sécurisé votre boit e-mail.</span></p>
            @endif
            </div>
        </td>
    </tr>
    <tr>
        <td align="center" height="100" width="400">
            <small style="color: grey;display: block;margin-top: 5px;">
                Si cet e-mail ne vous est pas destiné, merci de ne pas en tenir compte.
            </small>
        </td>
    </tr>
@endsection
