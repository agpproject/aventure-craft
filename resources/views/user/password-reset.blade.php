@extends('layouts.main')

@section('title', "Réinitialisez votre mot de passe")

@section('script')
    <script>
        window.scrollTo(0, 150);
    </script>
@endsection

@section('main')
    <section class="row">
        <div class="col bg-white p-3">
            <h1>Réinitialisez votre mot de passe.</h1>
            <p>Entrez l'adresse e-mail lié à votre compte afin de réinitialiser votre mot de passe.</p>
            @if (session('error'))
                <p class="text-danger">{{ session('error') }}</p>
            @endif
            <form action="{{ route('user.reset.password') }}" method="post" class="row" style="max-width: 500px;">
                @csrf
                <fieldset class="form-group col-12 col-md-9">
                    <input type="email" name="userEmail" id="userEmail" placeholder="mon-email@exemple.fr" class="form-control" required>
                </fieldset>
                <fieldset class="form-group text-right col-12 col-md-3">
                    <button type="submit" class="btn btn-secondary">Envoyer</button>
                </fieldset>
            </form>
        </div>
    </section>

@endsection
