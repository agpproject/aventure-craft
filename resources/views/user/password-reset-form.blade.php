@extends('layouts.main')

@section('title', "Réinitialiser votre mot de passe !")

@section('script')
    <script src="{{ asset('js/dist/password-form.js') }}"></script>
 @endsection

@section('main')
    <section class="row">
            <div class="col bg-white p-3">
                <h1>Réinitialiser votre mot de passe maintenant.</h1>
                <hr>
                @if ($errors->any())
                    <div role="alert" class="alert alert-warning">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                <form id="passwordForm" action="{{ route('user.reset.password.update') }}" method="post"
                      style="max-width: 400px; margin: 0 auto;">
                    @csrf
                    @method('put')
                    <input type="hidden" name="user" value="{{ $user }}">
                    <fieldset class="form-group">
                        <label for="password">Mot de passe</label>
                        <input type="password" class="form-control" name="password" id="password" aria-describedby="passwordText">
                        <small id="passwordText" class="form-text text-muted">
                            Le mot de passe doit au moins contenir 8 caractères.
                        </small>
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="confPassword">Confirmation du mot de passe</label>
                        <input type="password" class="form-control" name="confPassword" id="confPassword">
                        <div id="passwordControllerBis">
                            <span id="low"></span>
                            <span id="medium"></span>
                            <span id="strong"></span>
                            <p></p>
                        </div>
                    </fieldset>
                    <fieldset class="form-group text-right">
                        <button type="submit" class="btn btn-primary btn-sm">Réinitialiser votre mot de passe</button>
                    </fieldset>
                </form>
            </div>
    </section>
@endsection
