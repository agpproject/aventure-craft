<!doctype html>
<html lang="fr">
    <head>
        <!-- meta -->
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <!-- title -->
        <title>@yield('title') | Aventure-Craft, Serveur Minecraft MMORPG</title>

        <!-- favicon -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">

        <!-- stylesheet -->
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('fontawesome/css/all.min.css') }}">

        <!-- jquery 3.3.1 -->
        <script src="{{ asset('js/helpers/jquery-3.3.1.min.js') }}"></script>
        <!-- parallax js -->
        <script src="{{ asset('js/helpers/parallax.min.js') }}"></script>
    </head>
    <body>
        <!-- Header -->
        <header id="page-header" class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('images/bg-header.jpg') }}">
            <nav id="menu" class="navbar navbar-expand-lg navbar-dark bg-brown fixed-top pt-2 pb-2 p-lg-0">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="container">
                    <div class="collapse navbar-collapse d-lg-flex justify-content-between" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('home') }}">Accueil</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('guide.home') }}">Guide</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('guide.server-rules') }}">Nos Règles</a>
                            </li>
                        </ul>
                        <div class="text-white">
                            Adresse serveur : <em>Aventure.craft.vg:25565</em>
                        </div>
                        <div>
                            @if(auth()->check())
                                <button type="button" class="btn btn-outline-warning mr-2" data-toggle="modal" data-target="#profileModal">Mon Compte</button>
                                <a href="{{ route('auth.logout') }}" class="btn btn-outline-danger">Déconnexion</a>
                            @else
                                <button type="button" class="btn btn-success mr-2" data-toggle="modal" data-target="#loginModal">Connexion</button>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#inscriptModal">Inscription</button>
                            @endif
                        </div>
                    </div>
                </div>
            </nav>
            <div id="logo-title">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('images/logo-title.png') }}" alt="logo-title">
                </a>
            </div>
        </header>
        <!-- End header -->

        <div class="container  mt-md-4 mb-4">
            <div class="row">
                <!-- Main -->
                <main class="col-12 col-md-9">
                    @yield('main')
                </main>
                <!-- End main -->
                <!-- Aside -->
                <aside class="d-none d-md-block col-md-3">
                    @yield('aside')
                    <section class="bg-white p-2 mb-4">
                        <a href="{{ route('download') }}"><img src="{{ asset('images/slider-jouer.png') }}" alt="slider-jouer"></a>
                    </section>
                    @if(auth()->check())
                    <section class="bg-white p-2 mb-4">
                        <!--<div class="row">
                            <div class="col">
                                <iframe id="mini-maps" class="w-100" height="300" src="http://www.averall.aventure-craft.com" frameborder="0" referrerpolicy="no-referrer-when-downgrade"></iframe>
                            </div>
                        </div>-->
                        <div class="row">
                            <div class="col">
                                <a id="maps-button" href="http://www.averall.aventure-craft.com" data-img="{{ asset('images') }}" target="_blank">
                                    <img src="{{ asset('images/map-off.png') }}" alt="map">
                                </a>
                            </div>
                        </div>
                    </section>
                    @endif
                    <section class="bg-white p-2 text-center">
                        <h5>Retrouvez-nous sur :</h5>
                        <div class="d-flex justify-content-around w-75 m-auto">
                            <a href="https://www.facebook.com/aventurecraftfr" target="_blank">
                                <i class="fab fa-facebook-square fa-2x" style="color: #3b5998;"></i>
                            </a>
                            <a href="https://twitter.com/AventureCraft_" target="_blank">
                                <i class="fab fa-twitter-square fa-2x" style="color: #33ccff;"></i>
                            </a>
                            <a href="https://discord.gg/QtjNDk9" target="_blank">
                                <i class="fab fa-discord fa-2x" style="color: #7289da;"></i>
                            </a>
                            <a href="https://www.youtube.com/user/easycraftyt" target="_blank">
                                <i class="fab fa-youtube fa-2x" style="color: #ff0000;"></i>
                            </a>
                        </div>
                    </section>
                    @if (auth()->check())
                    <section class="text-center mt-4">
                        <button class="btn btn-warning btn-block" data-toggle="modal"
                                data-target="#contactModal">
                            <strong>Nous contacter</strong>
                        </button>
                    </section>
                    @endif
                </aside>
                <!-- End aside -->
            </div>
        </div>

        <!-- Footer -->
        <footer class="text-center text-white">
            <p>Aventure-Craft&copy;2019 - Tous droits réservés - <a href="{{ route('mentions') }}">Mentions
                    légales</a></p>
        </footer>
        <!-- End footer -->

        <!-- Modals -->
        @include('modals.contact-modal')
        @if(auth()->check())
            @include('modals.profile-modal')
            @include('modals.maps')
        @else
            @include('modals.log-modal')
            @include('modals.inscript-modal')
        @endif
        <!-- End modals -->

        <!-- js script -->
        <script src="{{ asset('js/helpers/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/helpers/popper.min.js') }}"></script>
        <script src="{{ asset('js/dist/app.js') }}"></script>
        @yield('script')
    </body>
</html>
