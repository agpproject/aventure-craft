@extends('layouts.main')

@section('script')
    <script>
        window.scrollTo(0, 500);

        let start = 10;
        let i = 0;
        let chrono_content = $('#chrono');

        $chrono = setInterval(function ()
        {
            $('#chrono').text(start - i);
            i++;
            if (i >= start)
                window.location.href = $('#message').data('route');
        }, 1000);
    </script>
@endsection

@section('main')
    <section id="message" class="row bg-white" data-route="{{ route('home') }}">
        <div class="col p-4">
            @yield('content')
            <hr>
            <p>Si vous avez des problème avec le site, contactez-nous !</p>
            <p>Vous allez être redirigé vers la page d'accueil dans <span id="chrono" class="text-primary"></span>...</p>
        </div>
    </section>
@endsection
