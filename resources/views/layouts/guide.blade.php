@extends('layouts.base')

@section('title', "Le Guide")

@section('aside')
    <nav class="nav flex-column mb-4">
        <a href="{{ route('guide.home') }}" class="nav-link">Accueil</a>
        <a href="{{ route('guide.server-rules') }}" class="nav-link">Nos règles</a>
    </nav>
@endsection
