<!doctype html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>@yield('title') | {{ config('app.name') }}</title>
    <body style="background-color: #333;font-family: 'sans-serif';">
        <table align="center" cellpadding="0" cellspacing="0" width="500" bgcolor="#fff">
            <tr>
                <td id="header" align="center" height="150">
                    <a href="{{ route('home') }}" target="_blank">
                        <img data-src="{{ asset('images/logo-title.png') }}" src="{{ asset('images/logo-title.png') }}" alt="bg-header" width="260">
                    </a>
                </td>
            </tr>
            @yield('content')
            <tr>
                <td align="center" height="50">
                    <small>
                        <a href="{{ route('home') }}" target="_blank">Aventure-Craft</a>&copy;2019-Tous droits réservés.
                    </small>
                </td>
            </tr>
        </table>
    </body>
</html>
