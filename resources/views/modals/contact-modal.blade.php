<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Nous contacter</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-3">
                <form id="contactForm" action="{{ route('contact') }}" method="post">
                    @csrf
                    <fieldset class="form-group">
                        <label for="contactEmail">Votre adresse e-mail</label>
                        <input type="email" class="form-control" name="contactEmail" id="contactEmail"
                               placeholder="mon-email@exemple.fr">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="contactSubject">Sujet du message</label>
                        <input type="text" class="form-control" name="contactSubject" id="contactSubject">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="contactMessage">Votre message</label>
                        <textarea class="form-control" name="contactMessage" id="contactMessage" rows="8"></textarea>
                    </fieldset>
                    <fieldset class="form-group text-center">
                        <button type="submit" class="btn btn-success btn-block">Envoyer</button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
