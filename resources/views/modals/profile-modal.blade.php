<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="profileModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Inscription</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-3">
                <p class="text-danger small text-right">
                    * Champs obligatoires
                </p>
                <form id="profileForm" action="{{ route('user.update') }}" method="post">
                    @csrf
                    @method('put')
                    @if(auth()->check())
                        <input type="hidden" id="user-id" name="user" value="{{ encrypt(auth()->user()->email) }}">
                    @endif
                    <fieldset class="form-group">
                        <label for="username">Pseudo Mincraft <span class="text-danger">*</span></label>
                        <input type="text" name="username" id="username" class="form-control" value="{{ auth()->user()->username }}">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="email">Adresse e-mail <span class="text-danger">*</span></label>
                        <input type="email" class="form-control" name="email" id="email" value="{{ auth()->user()->email }}">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="birthday">Date de naissance <span class="text-danger">*</span></label>
                        <input type="date" class="form-control" name="birthday" id="birthday" value="{{ auth()->user()->birthday }}">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="password">Mot de passe</label>
                        <input type="password" class="form-control" name="password" id="password" aria-describedby="passwordText">
                        <small id="passwordText" class="form-text text-muted">
                            Le mot de passe doit au moins contenir 8 caractères.
                        </small>
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="confPassword">Confirmation du mot de passe</label>
                        <input type="password" class="form-control" name="confPassword" id="confPassword">
                        <div id="passwordController">
                            <span id="low"></span>
                            <span id="medium"></span>
                            <span id="strong"></span>
                            <p></p>
                        </div>
                    </fieldset>
                    <fieldset class="form-group text-center">
                        <button type="submit" class="btn btn-outline-primary btn-block btn-sm">Mettre à jour</button>
                    </fieldset>
                </form>
                <form action="{{ route('user.delete') }}" method="post">
                    @csrf
                    @method('delete')
                    <input type="hidden" name="user" value="{{ encrypt(auth()->id()) }}">
                    <fieldset class="form-group text-center">
                        <button type="submit" class="btn btn-outline-danger btn-block btn-sm">Supprimer le compte</button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
