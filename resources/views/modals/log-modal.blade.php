<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Connexion</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-3">
                <form id="logForm" action="{{ route('auth.login') }}" method="post">
                    @csrf
                    <fieldset class="form-group">
                        <label for="logEmail">Adresse e-mail</label>
                        <input type="email" class="form-control" name="logEmail" id="logEmail" placeholder="mon-email@exemple.fr">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="logPassword">Mot de passe</label>
                        <input type="password" class="form-control" name="logPassword" id="logPassword">
                    </fieldset>
                    <fieldset class="form-group text-center">
                        <button type="submit" class="btn btn-success btn-block">Se Connecter</button>
                    </fieldset>
                </form>
                <div class="row">
                    <div class="col text-right">
                        <a href="{{ route('user.reset.password.view') }}">Mot de passe oublié ?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
