<div class="modal fade" id="inscriptModal" tabindex="-1" role="dialog" aria-labelledby="inscriptModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Inscription</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-3">
                <p class="text-danger small text-right">
                    * Champs obligatoires
                </p>
                <form id="inscriptForm" action="{{ route('user.inscription') }}" method="post">
                    @csrf
                    <fieldset class="form-group">
                        <label for="username">Pseudo Mincraft <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="username" id="username" placeholder="pseudo1234">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="email">Adresse e-mail <span class="text-danger">*</span></label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="mon-email@exemple.fr">
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="birthday">Date de naissance <span class="text-danger">*</span></label>
                        <input type="date" class="form-control" name="birthday" id="birthday" aria-describedby="birthdayText">
                        <small id="birthdayText" class="form-text text-muted">
                            Attention ! Vous devez être âgé d'au moins 14 ans pour vous inscrire.
                        </small>
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="password">Mot de passe <span class="text-danger">*</span></label>
                        <input type="password" class="form-control" name="password" id="password" aria-describedby="passwordText">
                        <small id="passwordText" class="form-text text-muted">
                            Le mot de passe doit au moins contenir 8 caractères.
                        </small>
                    </fieldset>
                    <fieldset class="form-group">
                        <label for="confPassword">Confirmation du mot de passe <span class="text-danger">*</span></label>
                        <input type="password" class="form-control" name="confPassword" id="confPassword">
                        <div id="passwordController">
                            <span id="low"></span>
                            <span id="medium"></span>
                            <span id="strong"></span>
                            <p></p>
                        </div>
                    </fieldset>
                    <fieldset class="form-group text-center">
                        <button type="submit" class="btn btn-primary btn-block">S'inscrire</button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
