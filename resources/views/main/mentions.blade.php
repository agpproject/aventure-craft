@extends('layouts.main')

@section('title', "Mentions Légales")

@section('main')
    <section class="bg-white p-3">
        <div class="row">
            <div class="col">
                <h1>Mentions Légales</h1>
                <hr>
            </div>
        </div>
        <article class="row">
            <div class="col">
                <h2>L'Hébergeur :</h2>
                <ul>
                    <li>OVH</li>
                    <li>2 rue Kellermann - 59100 Roubaix - France</li>
                    <li>Web : <a target="_blank" href="https://www.ovh.com/fr/">https://www.ovh.com/fr/</a></li>
                    <li>Téléphone : 1007</li>
                </ul>
            </div>
        </article>
        <article class="row mt-4">
            <div class="col text-justify">
                <h2>Droit d'auteur</h2>
                <p>
                    Le site et chacun des éléments, y compris mais sans limitation les marques, les logos, icônes,
                    infographies, photographies, qui le composent sont protégés au titre de la législation
                    internationale de la propriété intellectuelle. Les contenus figurant sur le site sont la
                    propriété d'Aventure-Craft et de ses créateurs.
                </p>
                <p>
                    Toute utilisation, reproduction ou représentation, par quelque procédé que ce soit, et sur
                    quelque support que ce soit, de tout ou partie du site et/ou des éléments qui le composent n'est
                    pas autorisée sans le consentement expresse d'Aventure-Craft.
                </p>
            </div>
        </article>
        <article class="row mt-4">
            <div class="col text-justify">
                <h2>Données personnelles</h2>
                <p>
                    AventureCraft considère que vos coordonnées sont des données confidentielles, et s’engage à ne
                    pas les divulguer. Vous disposez d’un droit d’accès, de modification, de rectification et de
                    suppression des données qui vous concernent (article 34 de la loi Informatique et libertés n°
                    78-17 du 6 janvier 1978). Vous pouvez exercer ce droit en écrivant à <a href="mailto:contact@aventure-craft.com">contact@aventure-craft.com</a>.
                </p>
                <p>
                    Les données collectées par le biais des formulaires du site (pseudo minecraft, adresse e-mail, et
                    date de naissance) sont utilisées pour le fonctionnement de l'espace personnel et pour répondre
                    au demande des utilisateurs. Aucune autre utilisation n'est prévue avec ces données à ce jour.
                </p>
                <p>
                    Ces données ne sont pas communiquées à d’autres tiers, sous réserve d’une demande formulée par les autorités judiciaires ou administratives habilitées à formuler cette demande.
                </p>
            </div>
        </article>
        <article class="row mt-4">
            <div class="col text-justify">
                <h2>Cookie</h2>
                <p>
                    Les cookies installés sur votre ordinateur sont utilisés uniquement pour le fonctionnement
                    logiciel du site. Aucun traçage de votre navigation, ni aucune collecte de données ne sont
                    effectués avec.
                </p>
            </div>
        </article>
    </section>
@endsection
