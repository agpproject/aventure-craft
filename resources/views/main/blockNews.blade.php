<!-- News -->
<section class="row bg-white mt-4">
    <div class="col p-3" id="news">
        <h2>Les News</h2>
        <div class="row">
            <!-- Server news -->
            <section class="col-12 col-md-6" id="server">
                <h3>Serveur News</h3>
                <hr>
                <article class="p-2">
                    <h4>News 1</h4>
                    <p class="text-indent">
                        résumé de l'article ici.
                    </p>
                    <div class="text-right">
                        <a class="btn btn-primary btn-sm" href="#">En savoir +</a>
                    </div>
                </article>
                <hr>
                <article class="p-2">
                    <h4>News 2</h4>
                    <p class="text-indent">
                        résumé de l'article ici.
                    </p>
                    <div class="text-right">
                        <a class="btn btn-primary btn-sm" href="#">En savoir +</a>
                    </div>
                </article>
                <hr>
                <article class="p-2">
                    <h4>News 3</h4>
                    <p class="text-indent">
                        résumé de l'article ici.
                    </p>
                    <div class="text-right">
                        <a class="btn btn-primary btn-sm" href="#">En savoir +</a>
                    </div>
                </article>
            </section>
            <!-- End server news -->
            <!-- Averall news -->
            <section class="col-12 col-md-6" id="world">
                <h3>Averall</h3>
                <hr><article class="p-2">
                    <h4>News 1</h4>
                    <p class="text-indent">
                        résumé de l'article ici.
                    </p>
                    <div class="text-right">
                        <a class="btn btn-primary btn-sm" href="#">En savoir +</a>
                    </div>
                </article>
                <hr>
                <article class="p-2">
                    <h4>News 2</h4>
                    <p class="text-indent">
                        résumé de l'article ici.
                    </p>
                    <div class="text-right">
                        <a class="btn btn-primary btn-sm" href="#">En savoir +</a>
                    </div>
                </article>
                <hr>
                <article class="p-2">
                    <h4>News 3</h4>
                    <p class="text-indent">
                        résumé de l'article ici.
                    </p>
                    <div class="text-right">
                        <a class="btn btn-primary btn-sm" href="#">En savoir +</a>
                    </div>
                </article>
            </section>
            <!-- End averall news -->
        </div>
    </div>
</section>
<!-- End News -->
