@extends('layouts.main')

@section('title', "Téléchargements")

@section('main')
    <section class="row bg-white">
        <article class="col p-3">
            <main>
                <div class="row">
                    <div class="col">
                        <h2>Titre ici</h2>
                        <p>Autant de p que de paragraphe</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md mb-4 mb-md-0 text-center">
                        <a href="{{ $packURL }}" download="AventurePack.zip">
                            <img class="btn-img" src="{{ asset('images/slider-RessourcePack.png') }}" alt="slider-RessourcePack">
                        </a>
                    </div>
                    <div class="col-12 col-md text-center">
                        <a href="{{ $optiFineURL }}" download="OptiFine.jar">
                            <img class="btn-img" src="{{ asset('images/slider-OptiFine.png') }}" alt="slider-OptiFine">
                        </a>
                    </div>
                </div>
                <div class="row mt-4 mb-4">
                    <div class="col">
                        <img src="{{ asset('images/slider-barre.png') }}" alt="slider-barre-1">
                    </div>
                </div>
                @if(!auth()->check())
                <div class="row">
                    <div class="col">
                        <h2>Titre ici</h2>
                        <p>Autant de p que de paragraphe</p>
                    </div>
                </div>
                <div class="row">
                    <div class="offset-3 col-6 text-center">
                        <button type="button" class="btn" data-toggle="modal" data-target="#inscriptModal">
                            <img src="{{ asset('images/slider-inscription.png') }}" alt="slider-inscription">
                        </button>
                    </div>
                </div>
                <div class="row mt-4 mb-4">
                    <div class="col">
                        <img src="{{ asset('images/slider-barre.png') }}" alt="slider-barre-2">
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col text-center">
                        <a href="#">
                            <img class="btn-img" src="{{ asset('images/slider-jouer.png') }}" alt="slider-jouer-2">
                        </a>
                    </div>
                </div>
            </main>
        </article>
    </section>
@endsection
