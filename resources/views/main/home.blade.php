@extends('layouts.main')

@section('title', "Bienvenue à Averall !")

@section('main')
    <!-- Presentation -->
    <section class="row bg-white">
        <article class="col p-3" id="present">
            <header>
                <h1>Bienvenue sur Aventure Craft ! Terre d'aventure et de gloire !</h1>
            </header>
            <hr>
            <main>
                <p class="text-indent">
                    Le site est encore en construction.
                </p>
                <p class="text-indent">
                    La légende d'Averall commencera bientôt, soyez encore juste un
                    peu patient ; ).
                </p>
            </main>
        </article>
    </section>
    <!-- End presentation -->
@endsection
