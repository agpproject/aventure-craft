<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Use to display some informations to the users
 * Class HelpersController
 * @package App\Http\Controllers
 */

class HelpersController extends Controller
{
    /**
     * Display the email activation message
     * @return View
     */
    public function email_activation(): View
    {
        return view('helpers.email-activation');
    }

    /**
     * Display the password reset message
     * @return View
     */
    public function reset_password(): View
    {
        return view('helpers.reset-password', ['expire' => false, 'end' => false]);
    }

    /**
     * Display the finish message of the password reset
     * @return View
     */
    public function reset_password_end(): View
    {
        return view('helpers.reset-password', ['end' => true, 'expire' => false]);
    }

    /**
     * Display the expire message of the password reset
     * @return View
     */
    public function reset_password_expire(): View
    {
        return view('helpers.reset-password', ['expire' => true, 'end' => false]);
    }

    /**
     * Display the profile activation message
     * @param string $type
     * @return View
     */
    public function profile_activation($type = 'activation'): View
    {
        return view('helpers.profile', ['type' => $type]);
    }

    /**
     * Display the profile delete message
     * @param string $type
     * @return View
     */
    public function profile_delete($type = 'first'): View
    {
        $type = "delete-$type";
        return view('helpers.profile', ['type' => $type]);
    }
}
