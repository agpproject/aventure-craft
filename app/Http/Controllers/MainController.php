<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

/**
 * Class MainController
 * @package App\Http\Controllers
 */

class MainController extends Controller
{
    public function download(Request $request)
    {
        $packURL = Storage::url('AventurePack.zip');
        $optiFineURL = Storage::url('OptiFine.jar');

        return view('main.download', compact('packURL', 'optiFineURL'));
    }
    /**
     * Display the main home page
     * @return View
     */
    public function home(): View
    {
        return view('main.home');
    }

    /**
     * Display the legals mentions page
     * @return View
     */
    public function mentions(): View
    {
        return view('main.mentions');
    }

    /**
     * Use to send a email to the administrator of the application
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function contact(Request $request)
    {
        // Create a Validator object of the $_POST variable
        $validate = Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'subject' => ['required', 'string'],
            'message' => ['required', 'string']
        ]);

        // Use to test the validation of the $_POST variable, returns an error if it isn't validate
        $validate->validate();

        $email = $request->input('email');
        $title = $request->input('subject');
        $text = $request->input('message');

        // Sending the email
        Mail::to('contact@aventure-craft.com')->send(new Contact($email, $title, $text));

        return response()->json([
            'success' => 'Envoi réussi, nous vous recontacterons bientôt'
        ]);
    }
}
