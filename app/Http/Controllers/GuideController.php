<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Controller of the guide pages
 * Class GuideController
 * @package App\Http\Controllers
 */

class GuideController extends Controller
{
    /**
     * Display the guide home page
     * @return View
     */
    public function home(): View
    {
        return view('guide.home');
    }

    /**
     * Display the guide server rules page
     * @return View
     */
    public function server_rules(): View
    {
        return view('guide.server-rules');
    }
}
