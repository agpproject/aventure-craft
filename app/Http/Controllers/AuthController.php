<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Use to authenticate on the application.
 * Class AuthController
 * @package App\Http\Controllers
 */

class AuthController extends Controller
{
    /**
     * Use to login.
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        // Test the $_POST['email'] and $_POST['password'] variables
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required', 'min:8']
        ]);

        // Make a associative array with the $_POST variables email and password
        $credential = $request->only('email', 'password');

        if (Auth::attempt($credential))
            return response()->json([
                'success' => route('home')
            ]);
        else
            return response(json_encode([
                'errors' => 'Adresse e-mail et/ou mot de passe incorrect'
            ]), 403);
    }

    /**
     * Use to logout.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        Auth::logout();
        return back();
    }
}
