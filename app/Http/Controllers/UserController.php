<?php

namespace App\Http\Controllers;

use App\Notifications\DeleteAccount;
use App\Notifications\EmailActivation;
use App\Notifications\ResetPassword;
use App\Notifications\Support;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Use to control the user profile
 * Class UserController
 * @package App\Http\Controllers
 */

class UserController extends Controller
{
    /**
     * Use to control the address email verification
     * @param $token
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function activation($token)
    {
        // Decode the verification token
        $token = json_decode(decrypt($token));

        // Get the user in the database
        $user = User::findWithEmail($token->email);

        // If the user is'nt found in the database, make a HTTP error 500
        if ($user === null)
            return abort(500);

        if ($token->time >= time()) {
            // If the token is'nt expire
            // Update the email verification field in the database
            $user->email_verified_at = now();
            $user->save();
            // Redirect to the profile activation page in HelpersController
            return redirect()->route('helpers.profile.activation');
        } else {
            // If the token is expire
            // Remake a new token
            $token = $this->gen_user_token($user->email);
            // Send an other activation email
            $user->notify(new EmailActivation($token));
            // Send to the profile activation page in HelpersController with the expire type
            return redirect()->route('helpers.profile.activation', ['type' => 'expire']);
        }
    }

    /**
     * Use to delete profile
     * @param Request $request
     * @param null $token
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function delete(Request $request, $token = null)
    {
        if ($request->isMethod('delete')) {
            // If delete request method is used
            // Test the $_POST variable
            $request->validate([
                'user' => ['required', 'string']
            ]);

            // Get the user in the database
            $user = User::find(decrypt($request->input('user')));
            // Generate a token with the user's email
            $token = $this->gen_user_token($user->email);
            // Send the delete profile confirmation email to the user
            $user->notify(new DeleteAccount($token));
            // Redirect to the delete profile page in HelpersController
            return redirect()->route('helpers.profile.delete');
        } elseif ($request->isMethod('get') && $token !== null) {
            // If get request method is used
            // Decode the token
            $token = json_decode(decrypt($token));
            // Get a user in the database with his email
            $user = User::findWithEmail($token->email);

            if ($user === null)
                return abort(500);

            if ($token->time >= time()) {
                $user->delete();
                return redirect()->route('helpers.profile.delete', ['type' => 'end']);
            } else
                return redirect()->route('helpers.profile.delete', ['type' => 'expire']);
        } else
            return abort(500);
    }

    public function inscription(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => ['required', 'email', 'unique:users,email'],
            'username' => ['required', 'unique:users,username', 'alpha_num'],
            'password' => ['required', 'min:8', 'same:confPassword'],
            'birthday' => ['required', 'date']
        ]);

        $validate->validate();

        $current_time = now();
        $major_date = new \DateTime($current_time->year - 18 . '-' . $current_time->month . '-' . $current_time->day);

        if ($request->input('birthday') > $major_date)
            return response(json_encode([
                'erros' => "Vous devez avoir au moins 18 pour vous inscrire"
            ]), 403);
        else {
            $user = new User();

            $user->email = $request->input('email');
            $user->username = strtolower($request->input('username'));
            $user->birthday = $request->input('birthday');
            $user->password = Hash::make($request->input('password'));

            $user->save();

            $token = $this->gen_user_token($user->email);
            $user->notify(new EmailActivation($token));

            return response()->json([
                'success' => route('helpers.email.activation')
            ]);
        }
    }

    public function password_reset(Request $request)
    {
        if ($request->isMethod('get'))
            return view('user.password-reset');
        elseif ($request->isMethod('put')) {
            $request->validate([
                'user' => ['required', 'string'],
                'password' => ['required', 'min:8', 'same:confPassword']
            ]);

            $user = User::find(decrypt($request->input('user')));

            $user->password = Hash::make($request->input('password'));
            $user->save();

            $user->notify(new Support('reset-end'));

            return redirect()->route('helpers.reset.password.end');
        } else if ($request->isMethod('post')) {
            $request->validate([
                'userEmail' => ['required', 'email']
            ]);

            $user = User::findWithEmail($request->input('userEmail'));

            if ($user === null)
                return redirect()->route('user.reset.password.view')->with('error', "Ce compte n'existe pas.");

            $token = $this->gen_user_token($user->email);
            $user->notify(new ResetPassword($token));

            return redirect()->route('helpers.reset.password');
        } else
            return abort(500);
    }

    public function password_reset_end(string $token)
    {
        $token = json_decode(decrypt($token));
        $user = User::findWithEmail($token->email);

        if ($user === null)
            return abort(500);

        if ($token->time >= time()) {
            return view('user.password-reset-form', ['user' => encrypt($user->id)]);
        } else {
            return redirect()->route('helpers.reset.password.expire');
        }
    }

    public function update(Request $request)
    {
        Validator::make($request->only('user'), [
            'user' => ['required', 'string']
        ]);

        $user = User::findWithEmail(decrypt($request->input('user')));

        if ($user->email !== $request->input('email')) {
            $request->validate([
                'email' => ['required', 'email', 'unique:users,email']
            ]);
            $user->email = $request->input('email');
        }
        if ($user->username !== $request->input('username')) {
            $request->validate([
                'username' => ['required', 'alpha_num', 'unique:users,username']
            ]);
            $user->username = strtolower($request->input('username'));
        }
        if ($user->birthday !== $request->input('birthday')) {
            $request->validate([
                'birthday' => ['required', 'date']
            ]);
            $user->birthday = $request->input('birthday');
        }
        if (!empty($request->input('password'))) {
            $request->validate([
                'password' => ['required', 'min:8', 'same:confPassword']
            ]);
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();

        return response()->json([
            'success' => "hello"
        ]);
    }
}
