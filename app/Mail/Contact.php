<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    private $email;
    private $title;
    private $text;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $email, string $title, string $text)
    {
        $this->email = $email;
        $this->title = $title;
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.username'), 'No_reply_Aventure_Craft')
                    ->subject('Un utilisateur a envoyé une demande')
                    ->view('mails.contact')
                    ->with([
                        'email' => $this->email,
                        'subject' => $this->title,
                        'text' => $this->text
                    ]);
    }
}
